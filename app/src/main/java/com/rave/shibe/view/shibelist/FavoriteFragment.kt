package com.rave.shibe.view.shibelist

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rave.shibe.databinding.FragmentShibeListBinding
import com.rave.shibe.model.ShibeRepo
import com.rave.shibe.viewmodel.ShibeViewModel

class FavoriteFragment : Fragment() {

    private var _binding: FragmentShibeListBinding? = null
    private val binding get() = _binding!!
    private val repo by lazy {ShibeRepo(requireContext())}
    private val shibeViewModel by viewModels<ShibeViewModel>() {
        ShibeViewModel.ShibeViewModelFactory(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        with(binding) {
            btnFavorite.text = "Return Home"
            btnLinear.setTextColor(Color.MAGENTA)


            btnFavorite.setOnClickListener {
                it.findNavController().navigate(FavoriteFragmentDirections.actionFavoriteFragmentToShibeListFragment())
            }
            btnLinear.setOnClickListener {
                rvShibeList.layoutManager = LinearLayoutManager(root.context)
                btnLinear.setTextColor(Color.MAGENTA)
                btnStaggered.setTextColor(Color.WHITE)
                btnGrid.setTextColor(Color.WHITE)
            }
            btnGrid.setOnClickListener {
                rvShibeList.layoutManager = GridLayoutManager(root.context, 3)
                btnLinear.setTextColor(Color.WHITE)
                btnStaggered.setTextColor(Color.WHITE)
                btnGrid.setTextColor(Color.MAGENTA)
            }
            btnStaggered.setOnClickListener {
                rvShibeList.layoutManager = StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)
                btnLinear.setTextColor(Color.WHITE)
                btnStaggered.setTextColor(Color.MAGENTA)
                btnGrid.setTextColor(Color.WHITE)
            }
        }

        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.run {
                progress.isVisible = state.isLoading
                rvShibeList.adapter = ShibeAdapter(state.shibes.filter { it.favorite }, repo)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}